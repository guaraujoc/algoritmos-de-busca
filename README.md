## Search Algorithms

Author: Gustavo Carvalho Araujo

You can reach me through my LinkedIn profile: [Gustavo Araújo](https://www.linkedin.com/in/guaraujoc/)

## Description

&nbsp;&nbsp;&nbsp;&nbsp;The program is an implementation of diferent ways to search datas in C.

# Instructions

&nbsp;&nbsp;&nbsp;&nbsp;In the first file, you will find various simple search algorithms, each of which prints the time taken to find a specific word. This allows for easy comparison between them. The second file, on the other hand, utilizes different hashing techniques to perform searches.

&nbsp;&nbsp;&nbsp;&nbsp;Within these files, you'll find sample data files that can be used by the program. However, it is also possible to incorporate other types of data without compromising the functionality of the program.


## Compilation Requirements

&nbsp;&nbsp;&nbsp;&nbsp;C Compiler compatible with C11 or higher standard.

## Project Structure

&nbsp;&nbsp;&nbsp;&nbsp;The project is structured as follows:

&nbsp;&nbsp;&nbsp;&nbsp;The file "main.c" contains the main function of the program.

&nbsp;&nbsp;&nbsp;&nbsp;The file "list.h" is a header file that defines data structures and function prototypes used in the program.

&nbsp;&nbsp;&nbsp;&nbsp;The file "list.c" contains the implementations of the functions defined in the header file "list.h."

&nbsp;&nbsp;&nbsp;&nbsp;Other source code files can be added to implement additional functionalities.

&nbsp;&nbsp;&nbsp;&nbsp;The code has been implemented using Doubly Linked Lists, which makes it highly compatible with adding new functions and providing functions for use in other codebases.

&nbsp;&nbsp;&nbsp;&nbsp;The "Makefile" contains instructions for compiling the program.

## Contributing

&nbsp;&nbsp;&nbsp;&nbsp;Contributions are welcome! If you have suggestions, improvements, or corrections, please feel free to open an issue or submit a pull request.

## Support

&nbsp;&nbsp;&nbsp;&nbsp;Email: ggustavo2001@gmail.com

&nbsp;&nbsp;&nbsp;&nbsp;LinkedIn profile: [Gustavo Araújo](https://www.linkedin.com/in/guaraujoc/) 

## License

&nbsp;&nbsp;&nbsp;&nbsp;This program is licensed under the [MIT License](https://opensource.org/licenses/MIT).


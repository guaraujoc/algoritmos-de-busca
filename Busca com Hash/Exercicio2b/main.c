/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cFiles/main.c to edit this template
 */

/* 
 * File:   main.c
 * Author: ggust
 *
 * Created on 7 de junho de 2023, 18:13
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h> // funções strcmp e strcpy
#include <math.h>

// Definição das variaveis que controlam a medição de tempo
clock_t _ini, _fim;

// Definição do tipo booleano
typedef unsigned char bool;

#define TRUE  1
#define FALSE 0


// Definição do tipo string
typedef char * string;

#define MAX_STRING_LEN 20

// Estrutura para armazenar a tabela hash
typedef struct {
    string* tabela;
    bool* ocupado;
    unsigned tamanho;
    unsigned colisao;
} hash_t;


unsigned converter(string s) {
    unsigned h = 0;
    for (int i = 0; s[i] != '\0'; i++)
        h = h * 256 + s[i];
    return h;
}

string* ler_strings(const char * arquivo, const int n)
{
    FILE* f = fopen(arquivo, "r");

    string* strings = (string *) malloc(sizeof(string) * n);

    for (int i = 0; !feof(f); i++) {
        strings[i] = (string) malloc(sizeof(char) * MAX_STRING_LEN);
        fscanf(f, "%s\n", strings[i]);
    }

    fclose(f);

    return strings;
}

void inicia_tempo()
{
    srand(time(NULL));
    _ini = clock();
}

double finaliza_tempo()
{
    _fim = clock();
    return ((double) (_fim - _ini)) / CLOCKS_PER_SEC;
}

unsigned h_div(unsigned x, unsigned i, unsigned B)
{
    return ((x % B) + i) % B;
}

unsigned h_mul(unsigned x, unsigned i, unsigned B)
{
    const double A = 0.6180;
    return  ((int) ((fmod(x * A, 1) * B) + i)) % B;
}


// Estabelece função rehash
unsigned rehash(unsigned x, unsigned i, unsigned B)
{
    return(h_mul(x, i, B) + i*h_div(x, i, B)) % B;
}

// Função para criar uma tabela hash vazia
hash_t criar_tabela(unsigned tamanho) {

    hash_t t; // insere estrutura da tabela hash
    t.tabela = (string*)malloc(sizeof(string) * tamanho);   // aloca memória
    t.ocupado = (bool*)calloc(tamanho, sizeof(bool)); // aloca memória para o controle de posições ocupadas
    t.tamanho = tamanho;  // define o tamanho da tabela
    t.colisao = 0; // inicializa a contagem de colisões
    return t;  // retorna a tabela

}

void remove_tabela(hash_t t) {
    free(t.tabela);   // libera a memória da tabela
    free(t.ocupado);  // libera memória das posições ocupadas
}

void insere(hash_t* t, string x, unsigned (*tipo_hash)(unsigned, unsigned, unsigned)) {

    unsigned pos, i = 0;
    unsigned y = converter(x);  // converte a string
    pos = tipo_hash(y, i, t->tamanho);

    for (i = 0; i < t->tamanho;i++) {

        //pos = tipo_hash(y, i, t->tamanho);  // usa a função hash para determinar a posição

        if (t->ocupado[pos] != TRUE) {    // verifica se a posição está disponível
            t->ocupado[pos] = TRUE;   // marca a posição como ocupada
            t->tabela[pos] = x;    // armazena o valor na posição da tabela
            return;   // sucesso
        }
        
        if(i == 0)
            t->colisao++;// houve colisão e precisou realizar rehash duplo[
        
        pos = rehash(y, i, t->tamanho);
    

    }
}

bool busca(hash_t* t, string x, unsigned (*tipo_hash)(unsigned, unsigned, unsigned)) {

    unsigned pos, i = 0;
    unsigned y = converter(x);

    pos = tipo_hash(y, i, t->tamanho);  // calcula a posição usando a função hash

    for(i = 0; t->ocupado[pos] && i < t->tamanho; i++) {  // loop para percorrer posições e encontrar o elemento

        if(t->ocupado[pos] == TRUE && strcmp(t->tabela[pos], x) == 0)
            return TRUE; // elemento encontrado

        if(t->ocupado[pos] == FALSE) // alcançou uma posição vazia e não encontrou, finaliza o loop
            return FALSE;

        pos = rehash(y, i, t->tamanho); // faz rehash e refaz a pesquisa

    }

}

int main(int argc, char const *argv[]) {

    unsigned N = 50000;
    unsigned M = 70000;
    unsigned B = 150001;

    unsigned colisoes_h_div;
    unsigned colisoes_h_mul;

    unsigned encontrados_h_div = 0;
    unsigned encontrados_h_mul = 0;

    string *insercoes = ler_strings("strings_entrada.txt", N);
    string *consultas = ler_strings("strings_busca.txt", M);

    // criar tabela hash da divisão
    hash_t tabela_div = criar_tabela(B);
    // inserção dos dados na tabela hash usando hash por divisão
    inicia_tempo();
    for (int i = 0; i < N; i++) {
        insere(&tabela_div, insercoes[i], h_div);

    }

    double tempo_insercao_h_div = finaliza_tempo();

    // consulta dos dados na tabela hash usando hash por divisão
    inicia_tempo();

    for (int i = 0; i < M; i++) {

        // buscar consultas[i] na tabela hash
        if(busca(&tabela_div, consultas[i], h_div))
            encontrados_h_div++;

    }

    double tempo_busca_h_div = finaliza_tempo();

    colisoes_h_div = tabela_div.colisao;
    // limpa a tabela hash com hash por divisão

    remove_tabela(tabela_div);


    // cria tabela hash com hash por multiplicação
    hash_t tabela_mul = criar_tabela(B);

    // inserção dos dados na tabela hash usando hash por multiplicação
    inicia_tempo();
    for (int i = 0; i < N; i++) {

        insere(&tabela_mul, insercoes[i], h_mul);

    }

    double tempo_insercao_h_mul = finaliza_tempo();

    // busca dos dados na tabela hash com hash por multiplicação
    inicia_tempo();
    for (int i = 0; i < M; i++) {

        // buscar consultas[i] na tabela hash
        if(busca(&tabela_mul, consultas[i], h_mul))
            encontrados_h_mul++;

    }
    double tempo_busca_h_mul = finaliza_tempo();

    colisoes_h_mul = tabela_mul.colisao;

    // limpa a tabela hash com hash por multiplicação

    remove_tabela(tabela_mul);

    printf("Hash por Divisão\n");
    printf("Colisões na inserção: %d\n", colisoes_h_div);
    printf("Tempo de inserção   : %fs\n", tempo_insercao_h_div);
    printf("Tempo de busca      : %fs\n", tempo_busca_h_div);
    printf("Itens encontrados   : %d\n", encontrados_h_div);
    printf("\n");
    printf("Hash por Multiplicação\n");
    printf("Colisões na inserção: %d\n", colisoes_h_mul);
    printf("Tempo de inserção   : %fs\n", tempo_insercao_h_mul);
    printf("Tempo de busca      : %fs\n", tempo_busca_h_mul);
    printf("Itens encontrados   : %d\n", encontrados_h_mul);

    return 0;
}


/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cFiles/main.c to edit this template
 */

/* 
 * File:   main.c
 * Author: ggust
 *
 * Created on 7 de junho de 2023, 22:33
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <math.h>

// Definição das variaveis que controlam a medição de tempo
clock_t _ini, _fim;

// Definição do tipo booleano
typedef unsigned char bool;
#define TRUE  1
#define FALSE 0

// Definição do tipo string
typedef char* string;

#define MAX_STRING_LEN 20

typedef struct lista_colisoes {
    string dados;
    struct lista_colisoes* i;
} lista_colisoes;

typedef struct {
    string* tabela;
    lista_colisoes** lista_colisao;
    bool* ocupado;
    unsigned tamanho;
    unsigned colisao;
} hash_t;

unsigned converter(string s) {
    unsigned h = 0;
    for (int i = 0; s[i] != '\0'; i++)
        h = h * 256 + s[i];
    return h;
}

string* ler_strings(const char* arquivo, const int n) {
    FILE* f = fopen(arquivo, "r");

    string* strings = (string*)malloc(sizeof(string) * n);

    for (int i = 0; !feof(f); i++) {
        strings[i] = (string)malloc(sizeof(char) * MAX_STRING_LEN);
        fscanf(f, "%s\n", strings[i]);
    }

    fclose(f);

    return strings;
}

void inicia_tempo() {
    srand(time(NULL));
    _ini = clock();
}

double finaliza_tempo() {
    _fim = clock();
    return ((double)(_fim - _ini)) / CLOCKS_PER_SEC;
}

unsigned h_div(unsigned x, unsigned B) {
    return x % B;
}

unsigned h_mul(unsigned x, unsigned B) {
    const double A = 0.6180;
    return fmod(x * A, 1) * B;
}

hash_t criar_tabela(unsigned tamanho) {
    hash_t t; // insere estrutura da tabela hash
    t.tabela = (string*)malloc(sizeof(string) * tamanho); // aloca memória
    t.lista_colisao = (lista_colisoes**)malloc(sizeof(lista_colisoes*) * tamanho); // aloca memória para as colisoes
    t.ocupado = (bool*)calloc(tamanho, sizeof(bool)); // aloca memória para o controle de posições ocupadas
    t.tamanho = tamanho;  // define o tamanho da tabela
    t.colisao = 0; // inicializa a contagem de colisões
    return t; // retorna a tabela
}

void remove_tabela(hash_t t) {
    free(t.tabela); // libera a memória da tabela
    free(t.ocupado);  // libera memória das posições ocupadas
    free(t.lista_colisao);  // libera a memória das colisoes
}

void insere(hash_t* t, string x, unsigned (*tipo_hash)(unsigned, unsigned)) {
    unsigned pos = tipo_hash(converter(x), t->tamanho);

    if (t->ocupado[pos] == FALSE) {  // verifica se a posição está disponível
        t->ocupado[pos] = TRUE;  // se sim, indica que está ocupada e insere
        t->tabela[pos] = x;
        return;  // sucesso
    }


    // caso contrário, insere em uma lista
    lista_colisoes* l = (lista_colisoes*)malloc(sizeof(lista_colisoes)); // aloca memória para um novo espaço na lista de colisoes
    l->dados = x; // atribui o valor na lista de colisoes
    l->i = t->lista_colisao[pos];  // liga a posição que ocorre a colisão com a tabela de colisões
    t->lista_colisao[pos] = l; // insere o elemento no início da lista
    t->colisao++; // alerta que houve colisão
}

bool busca(hash_t* t, string x, unsigned (*tipo_hash)(unsigned, unsigned)) {
    unsigned pos = tipo_hash(converter(x), t->tamanho);

    if (t->ocupado[pos] == TRUE && strcmp(t->tabela[pos], x) == 0) {  // verifica o elemento na sua posição hash
        return TRUE; // encontrado, retorna
    }

    // busca na lista de colisão
    lista_colisoes* l = t->lista_colisao[pos];
    while (l != NULL) { // verifica as posições da tabela de colisões do hash
        if (strcmp(l->dados, x) == 0) {
            return TRUE;
        }
        l = l->i; // atualiza o ponteiro para o próximo valor da lista
    }

    return FALSE; // valor não encontrado
}

int main(int argc, char const* argv[]) {
    const int N = 50000;
    const int M = 70000;
    const int B = 150001;

    unsigned colisoes_h_div = 0;
    unsigned colisoes_h_mul = 0;

    unsigned encontrados_h_div = 0;
    unsigned encontrados_h_mul = 0;

    string* insercoes = ler_strings("C:\\Users\\ggust\\EDII_P2_Clion\\strings_entrada.txt", N);
    string* consultas = ler_strings("C:\\Users\\ggust\\EDII_P2_Clion\\strings_busca.txt", M);

    // cria tabela hash com hash por divisão
    hash_t tabela_div = criar_tabela(B);

    // inserção dos dados na tabela hash com hash por divisão
    inicia_tempo();
    for (int i = 0; i < N; i++) {
        insere(&tabela_div, insercoes[i], h_div);
    }
    double tempo_insercao_h_div = finaliza_tempo();

    // busca dos dados na tabela hash com hash por divisão
    inicia_tempo();
    for (int i = 0; i < M; i++) {
        if (busca(&tabela_div, consultas[i], h_div)) {
            encontrados_h_div++;
        }
    }
    double tempo_busca_h_div = finaliza_tempo();

    colisoes_h_div = tabela_div.colisao;

    // destroi tabela hash com hash por divisão
    remove_tabela(tabela_div);

    // cria tabela hash com hash por multiplicação
    hash_t tabela_mul = criar_tabela(B);

    // inserção dos dados na tabela hash com hash por multiplicação
    inicia_tempo();
    for (int i = 0; i < N; i++) {
        insere(&tabela_mul, insercoes[i], h_mul);
    }
    double tempo_insercao_h_mul = finaliza_tempo();

    // busca dos dados na tabela hash com hash por multiplicação
    inicia_tempo();
    for (int i = 0; i < M; i++) {
        if (busca(&tabela_mul, consultas[i], h_mul)) {
            encontrados_h_mul++;
        }
    }
    double tempo_busca_h_mul = finaliza_tempo();

    colisoes_h_mul = tabela_mul.colisao;

    // destroi tabela hash com hash por multiplicação
    remove_tabela(tabela_mul);

    printf("Hash por Divisão\n");
    printf("Colisões na inserção: %d\n", colisoes_h_div);
    printf("Tempo de inserção   : %fs\n", tempo_insercao_h_div);
    printf("Tempo de busca      : %fs\n", tempo_busca_h_div);
    printf("Itens encontrados   : %d\n", encontrados_h_div);
    printf("\n");
    printf("Hash por Multiplicação\n");
    printf("Colisões na inserção: %d\n", colisoes_h_mul);
    printf("Tempo de inserção   : %fs\n", tempo_insercao_h_mul);
    printf("Tempo de busca      : %fs\n", tempo_busca_h_mul);
    printf("Itens encontrados   : %d\n", encontrados_h_mul);

    return 0;
}
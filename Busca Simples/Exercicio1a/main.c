/* 
 * File:   main.c
 * Author: Gustavo Carvalho Araújo
 * Created on 17 de maio de 2023, 21:08
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Definição das variaveis que controlam a medição de tempo
clock_t _ini, _fim;

// Definição do tipo booleano
unsigned char typedef bool;
#define TRUE  1
#define FALSE 0

// realiza a leitura dos números contidos no arquivo
int* ler_inteiros(const char * arquivo, const int n)
{
    FILE* f = fopen(arquivo, "r"); // abre o arquivo

    int * inteiros = (int *) malloc(sizeof(int) * n); // aloca memória

    for (int i = 0; !feof(f); i++)  // monta uma lista com os números no arquivo
        fscanf(f, "%d\n", &inteiros[i]);

    fclose(f); // fecha o arquivo

    return inteiros; // retorna a lista com os números
}

void inicia_tempo() // inicia o cálculo do tempo de execução
{
    srand(time(NULL)); // coloca uma semente aleatória
    _ini = clock(); // inicia o clock
}

double finaliza_tempo() // finaliza a contagem do tempo
{
    _fim = clock(); // finaliza o clock
    return ((double) (_fim - _ini)) / CLOCKS_PER_SEC; // retorna o tempo total
}

// realiza uma busca sequencial simples
int busca_sequencial_simples(const int * arquivo, const int n, const int buscado){

    for(int i = 0; i < n; i++){   // percorre a lista procurando o valor
        if(arquivo[i] == buscado)
            return TRUE; // se encontrar o valor, retorna verdadeiro
    }

    return FALSE; // valor não encontrado, retorna falso
}


int main(int argc, char const *argv[])
{
    const int N = 50000; // define o número de linhas a serem lidas
    unsigned encontrados = 0;

    int* entradas = ler_inteiros("inteiros_entrada.txt", N); // lê a lista de entrada
    int* consultas = ler_inteiros("inteiros_busca.txt", N); // lê a lista de busca

    // realiza busca sequencial
    inicia_tempo(); // inicia o clock
    for (int i = 0; i < N; i++) { // percorre a lista verificando quantos valores estão na lista

        if(busca_sequencial_simples(entradas, N, consultas[i]) == TRUE) // buscar o elemento consultas[i] na entrada
            encontrados++;

    }

    double tempo_busca = finaliza_tempo(); // finaliza o clock

    printf("Tempo de busca    :\t%fs\n", tempo_busca); // imprime o tempo da busca
    printf("Itens encontrados :\t%d\n", encontrados); // imprime o número de elementos consultas em entradas

    free(entradas); // libera a memória alocada para entradas
    free(consultas); // libera a memória alocada para consultas

    return 0; // sucesso
}


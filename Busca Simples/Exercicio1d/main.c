/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cFiles/main.c to edit this template
 */

/* 
 * File:   main.c
 * Author: ggust
 *
 * Created on 25 de maio de 2023, 10:59
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Definição das variaveis que controlam a medição de tempo
clock_t _ini, _fim;

typedef int elem;

typedef struct{
    elem chave;
    int posicao;
} indice;

// Definição do tipo booleano
unsigned char typedef bool;
#define TRUE  1
#define FALSE 0

// realiza a leitura dos números contidos no arquivo
int* ler_inteiros(const char * arquivo, const int n)
{
    FILE* f = fopen(arquivo, "r"); // abre o arquivo

    int * inteiros = (int *) malloc(sizeof(int) * n); // aloca memória

    for (int i = 0; !feof(f); i++)  // monta uma lista com os números no arquivo
        fscanf(f, "%d\n", &inteiros[i]);

    fclose(f); // fecha o arquivo

    return inteiros; // retorna a lista com os números
}

void inicia_tempo() // inicia o cálculo do tempo de execução
{
    srand(time(NULL)); // coloca uma semente aleatória
    _ini = clock(); // inicia o clock
}

double finaliza_tempo() // finaliza a contagem do tempo
{
    _fim = clock(); // finaliza o clock
    return ((double) (_fim - _ini)) / CLOCKS_PER_SEC; // retorna o tempo total
}

// realiza busca sequencial com indice
int busca_sequencial_indice(const int* arquivo, const int n, const int buscado, indice arquivo_i[], int t) {

    int i, j;

    // define indices
    for(i = 0; i < t; i++) {
        if(arquivo_i[i].chave > buscado)
            break;
    }

    if(i == 0)
        return FALSE;

    // realiza busca a partir dos indices
    for(j = arquivo_i[i - 1].posicao; j < n; j++) {
        if (arquivo[j] == buscado)
            return TRUE;

        if (arquivo[j] > buscado)
            return FALSE;
    }
}

int comparar(const void* a, const void* b) {
    int valor1 = *(int*)a;
    int valor2 = *(int*)b;

    if (valor1 < valor2) {
        return -1;
    } else if (valor1 > valor2) {
        return 1;
    } else {
        return 0;
    }
}

int main(int argc, char const *argv[])
{
    indice *arquivo_i;

    const int N = 50000; // define o número de linhas a serem lidas
    const int t = 10000; // define o intervalo na tabela de chaves
    unsigned encontrados = 0;

    int* entradas = ler_inteiros("inteiros_entrada.txt", N); // lê a lista de entrada
    int* consultas = ler_inteiros("inteiros_busca.txt", N); // lê a lista de busca
    
    qsort(entradas, N, sizeof(int), comparar);
    // realiza busca sequencial

    arquivo_i = malloc(sizeof(indice) * t);
    for(int i = 0; i < t; i++) {
        arquivo_i[i].posicao = i * (N/t);
        arquivo_i[i].chave = entradas[arquivo_i[i].posicao];
    }

    inicia_tempo(); // inicia o clock
    for (int i = 0;i < N; i++) { // percorre a lista verificando quantos valores estão na lista

        if (busca_sequencial_indice(entradas, N, consultas[i], arquivo_i, t) == TRUE) // buscar o elemento consultas[i] na entrada
            encontrados++;

    }

    double tempo_busca = finaliza_tempo(); // finaliza o clock

    printf("Tempo de busca    :\t%fs\n", tempo_busca); // imprime o tempo da busca
    printf("Itens encontrados :\t%d\n", encontrados); // imprime o número de elementos consultas em entradas

    free(entradas); // libera a memória alocada para entradas
    free(consultas); // libera a memória alocada para consultas
    free(arquivo_i);

    return 0; // sucesso
}

